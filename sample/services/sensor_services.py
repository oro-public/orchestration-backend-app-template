from rest_framework.response import Response
from rest_framework import status
from django.http import JsonResponse
import urllib
from urllib.request import urlopen
import requests,json

#################################################################################################################
# GET SENSOR DATA ###############################################################################################
#################################################################################################################


def get_sensors(data):
	try:
		link = "https://api.sensor.oronetworks.com/user/auth"               # auth url for login
		payload = {'username': 'username', 'password': 'password'}     # username and password is your actual credentials
		auth = requests.post(link, data=payload)
		data = json.loads(auth.text)
		token = "Bearer"+' '+data["token"]
		sensor_data=requests.get("https://api.sensor.oronetworks.com/lamp?offset=0&limit=20", headers={"Authorization":token}) #sensor url for getting sensor
		data = {"status_code": 200, "status": "success", "message": "List of sensors", "data":json.loads(sensor_data.text)}
		return JsonResponse(data=data,status=200)
	except:
		data = {"status_code": 400, "status": "error", "message": "Something wrong", "data":[]}
		return JsonResponse(data=data,status=200)
