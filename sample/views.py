from django.shortcuts import render
from .services import sensor_services
from rest_framework.generics import GenericAPIView

#################################################################################
# SENSOR LIST API ###############################################################
#################################################################################

class SensorList(GenericAPIView):
   
    def get(self, request):
        return sensor_services.get_sensors(request)